section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .cicle:
    	cmp byte [rdi+rax], 0;
    	jnz .inc_count
    	ret
    .inc_count:
    	inc rax
    	jmp .cicle

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov     rdx, rax
    mov     rsi, rdi
    mov     rax, 1
    mov     rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdi, 1
    mov rdx, 1
    mov rax, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r9, 10
    mov rax, 0
    mov rcx, 0
    dec rsp
    mov [rsp], byte 0
    mov rax, rdi
    .to_dec:
        mov rdx, 0
        div r9
        add dl, '0'
        dec rsp
        mov [rsp], dl
        inc rcx
        cmp rax, 0
        jnz .to_dec
    .print
    	mov rdi, rsp
    	inc rcx
    	push rcx
    	call print_string
    	pop rcx
    	add rsp, rcx
    	ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov r9, 0
    mov rcx, 0
    .cmp:
    	mov r9b, byte [rdi + rcx]
    	cmp r9b, byte [rsi + rcx]
    	jne .end
    	cmp r9, 0
    	jz .success
    	inc rcx
    	jmp .cmp
    .success:
    	mov rax, 1
    	ret
    .end:
    	mov rax, 0
    	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rdi
    mov r10, rsi
    mov r9, rdi
    .check:
	call read_char
	test al, al
	jz .fault
	cmp al, " "
	je .check
	cmp al, "\n"
	je .check
	cmp al, 0x9
	je .check
    .cicle:
        dec r10
	jz .fault
	mov byte[r9], al
	inc r9
	call read_char
	test al, al
	jz .exit
	cmp al, " "
	je .exit
	cmp al, "\n"
	je .exit
	cmp al, 0x9
	je .exit
	jmp .cicle
    .fault:
        add rsp, 8
	xor rax, rax
	xor rdx, rdx
        ret
    .exit:
        mov byte[r9], 0
	pop rax
	mov rdx, r9
	sub rdx, rax
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rax, 0
    mov r9, 0
    mov r10, 10
    mov rdx, 0
    .read_num:
    	mov r9b, [rdi + rdx]
    	cmp r9b, '9'
    	ja .fault
    	cmp r9b, '0'
    	jb .fault
    	sub r9b, '0'
    	push rdx
    	mul r10
    	pop rdx
    	add rax, r9
    	inc rdx
    	jmp .read_num
    .fault:
    	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
        jne .success
        inc rdi
        call parse_uint
        cmp rdx, 0
        je .fault
        inc rdx
        neg rax
        ret
    .fault:
        xor rax,rax
        ret
    .success:
        jmp parse_uint


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r10, r10
    xor rax, rax
    push rdi
    mov rdi, rsi
    pop rsi
    call string_length
    mov rcx, rax
    rep movsb
    mov rax, r10
    xor r10, r10
    ret
